#include "StateParser.h"

#include "BaseObject.h"
#include "TextureManager.h"

#include <iostream>

bool StateParser::parseState(const char* fileName, std::string stateID, std::vector<BaseObject*>* pObjects, std::vector<std::string>* pTextureIDs) {

	TiXmlDocument xmlDoc;

	if (!xmlDoc.LoadFile(fileName)) {
		std::cerr << xmlDoc.ErrorDesc() << "\n";
		return false;
	}

	TiXmlElement* pRoot = xmlDoc.RootElement();

	TiXmlElement* pStateRoot = 0;
	for (TiXmlElement* e = pRoot->FirstChildElement(); e != 0; e = e->NextSiblingElement()) {
		if (e->Value() == stateID)
			pStateRoot = e;
	}

	if (pStateRoot == 0) {
		std::cerr << "StateParser::parseState failed: Could not find state root " << stateID << "\n";
		return false;
	}

	TiXmlElement* pTextureRoot = 0;
	for (TiXmlElement* e = pStateRoot->FirstChildElement(); e != 0; e = e->NextSiblingElement()) {
		if (e->Value() == std::string("TEXTURES")) {
			pTextureRoot = e;
			break;
		}
	}

	if (pTextureRoot != 0)
		parseTextures(pTextureRoot, pTextureIDs);

	TiXmlElement* pObjectRoot = 0;
	for (TiXmlElement* e = pStateRoot->FirstChildElement(); e != 0; e = e->NextSiblingElement()) {
		if (e->Value() == std::string("OBJECTS")) {
			pObjectRoot = e;
			break;
		}
	}

	if (pObjectRoot != 0)
		parseObjects(pObjectRoot, pObjects);

	xmlDoc.Clear();
	
	return true;
}

void StateParser::parseTextures(TiXmlElement* pTextureRoot, std::vector<std::string>* pTextureIDs) {
	for (TiXmlElement* e = pTextureRoot->FirstChildElement(); e != 0; e = e->NextSiblingElement()) {
		std::string fileName = e->Attribute("fileName");
		std::string id = e->Attribute("ID");

		if (TextureManager::load(fileName.c_str(), id) != 0) {
			std::cerr << "StateParser::parseTextures failed: TextureManager::load failed to load texture " << fileName << "\n";
			return;
		}
		pTextureIDs->push_back(id);
	}
}

void StateParser::parseObjects(TiXmlElement* pObjectRoot, std::vector<BaseObject*>* pObjects) {
	for (TiXmlElement* e = pObjectRoot->FirstChildElement(); e != 0; e = e->NextSiblingElement()) {
		int x, y, width, height, numFrames, callbackID, animSpeed;
		std::string textureID;

		e->Attribute("x", &x);
		e->Attribute("y", &y);
		e->Attribute("width", &width);
		e->Attribute("height", &height);
		e->Attribute("numFrames", &numFrames);
		e->Attribute("callbackID", &callbackID);
		e->Attribute("animSpeed", &animSpeed);

		textureID = e->Attribute("textureID");

		BaseObject* pObject = GameObjectFactory::create(e->Attribute("type"));
		pObject->load(new LoaderParams(x, y, width, height, textureID, numFrames, animSpeed, callbackID));
		pObjects->push_back(pObject);
	}
}