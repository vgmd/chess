// VERSUSPLAYSTATE CANNOT use StateParser to
// create ChessPiece objects. I chose not to
// create a separate class file for each different
// chess piece, so instead I create them in the
// onEnter function. This turns out to be faster
// than manually adding them in StateFile.xml
// anyway.

#ifndef _PLAYSTATE_H
#define _PLAYSTATE_H

#include "BaseState.h"
#include "ChessPiece.h"

enum check_condition {
	NOCHECK = 0,
	CHECK = 1,
	CHECKMATE = 2
};

class PlayState : public BaseState {
	
public:
	virtual void render();
	virtual void update();
	virtual void onEnter();
	virtual void onExit();

	virtual std::string getStateID() { return s_stateID; }

protected:
	void setPiece(piece_type t, color c, int tileX, int tileY, int index);
	void flipTurn() {
		if (m_currentTurn == WHITE)
			m_currentTurn = BLACK;
		else
			m_currentTurn = WHITE;
	}
	void checkMove(int tileX, int tileY);
	void killPieceAt(int tileX, int tileY);

	// Move check functions
	bool checkPawnMove(int tileX, int tileY, bool* killMove = 0);
	bool checkRookMove(int tileX, int tileY, bool* castleLeft, bool* castleRight, bool* killMove = 0);
	bool checkKnightMove(int tileX, int tileY, bool* killMove = 0);
	bool checkBishopMove(int tileX, int tileY, bool* killMove = 0);
	bool checkQueenMove(int tileX, int tileY, bool* killMove = 0);
	bool checkKingMove(int tileX, int tileY, bool* castleLeft, bool* castleRight, bool* killMove = 0);

	check_condition check(color currentTurnColor);

	static std::string s_stateID;
	
	int m_origTileX;
	int m_origTileY;
	color m_currentTurn;
	ChessPiece* m_heldPiece;
	ChessPiece* m_board[8][8];
};

#endif