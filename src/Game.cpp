#include "Game.h"

/* Engine */
#include "GameObjectFactory.h"
#include "GameStateMachine.h"
#include "InputHandler.h"
#include "SDLErrorLogger.h"
#include "TextureManager.h"
#include "Vector2D.h"
/* ------ */

/* Game States */
#include "PlayState.h"
/* ----------- */

#include <SDL.h>

SDL_Renderer* Game::s_pRenderer;

int Game::init() {

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		logSDLError(std::cerr, "SDL_Init");
		return -1;
	}

	// Create the game window
	m_pWindow = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if (m_pWindow == 0) {
		logSDLError(std::cerr, "SDL_CreateWindow");
		return -2;
	}

	// Create the game renderer
	s_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (s_pRenderer == 0) {
		logSDLError(std::cerr, "SDL_CreateRenderer");
		return -3;
	}
	TextureManager::setRenderer(s_pRenderer);

	GameStateMachine::pushState(new PlayState());

	m_running = true;
	return 0;
}

void Game::handleEvents() {
	InputHandler::handleEvents();
	if (InputHandler::getQuit())
		m_running = false;
	if (InputHandler::isKeyDown(SDLK_ESCAPE))
		m_running = false;
}

void Game::update() {
	GameStateMachine::update();
}

void Game::render() {
	SDL_SetRenderDrawColor(s_pRenderer, 255, 255, 255, 255);
	SDL_RenderClear(s_pRenderer);

	GameStateMachine::render();

	SDL_RenderPresent(s_pRenderer);
}

void Game::clean() {
	TextureManager::clean();
	SDL_DestroyRenderer(s_pRenderer);
	SDL_DestroyWindow(m_pWindow);

	SDL_Quit();
}