#include "GameObjectFactory.h"

#include "BaseObject.h"

#include <iostream>

std::unordered_map<std::string, BaseCreator*> GameObjectFactory::s_creators;

GameObjectFactory::~GameObjectFactory() {
	for (std::unordered_map<std::string, BaseCreator*>::iterator it = s_creators.begin(); it != s_creators.end(); it++) {
		delete it->second;
	}
	s_creators.clear();
}

void GameObjectFactory::destroyType(std::string typeID) {
	std::unordered_map<std::string, BaseCreator*>::iterator it = s_creators.find(typeID);

	if (it == s_creators.end()) {
		std::cout << "GameObjectFactory::destroyType could not destroy " << typeID << " because it does not exist.\n";
		return;
	}

	delete it->second;

	s_creators.erase(it);
}

void GameObjectFactory::registerType(std::string typeID, BaseCreator* pCreator) {
	std::unordered_map<std::string, BaseCreator*>::iterator it = s_creators.find(typeID);

	if (it != s_creators.end()) {
		// Game object type already exists
		std::cout << "GameObjectFactory::registerType warning: " << typeID << " game object type already exists. Aborting.\n";
		delete pCreator;
		return;
	}

	s_creators[typeID] = pCreator;

	return;
}

BaseObject* GameObjectFactory::create(std::string typeID) {
	std::unordered_map<std::string, BaseCreator*>::iterator it = s_creators.find(typeID);

	if (it == s_creators.end()) {
		// Game object type doesn't exist!
		std::cerr << "GameObjectFactory::create cannot find object of type " << typeID << "\n";
		return 0;
	}

	BaseCreator* pCreator = it->second;
	return pCreator->createGameObject();
}