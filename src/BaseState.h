#ifndef _BASESTATE_H
#define _BASESTATE_H

#include "BaseObject.h"
#include "GameStateMachine.h"
#include "StateParser.h"

#include <vector>
#include <string>

class BaseState {

public:
	virtual ~BaseState() {
		for (size_t i = 0; i < m_gameObjects.size(); i++) {
			delete m_gameObjects[i];
		}
		m_gameObjects.clear();
	}

	virtual void render() {
		for (size_t i = 0; i < m_gameObjects.size(); i++) {
			m_gameObjects[i]->render();
		}
	}
	virtual void update() {
		for (size_t i = 0; i < m_gameObjects.size(); i++) {
			if (GameStateMachine::changingState())
				break;
			m_gameObjects[i]->update();
		}
	}

	virtual void onEnter() =0;
	virtual void onExit() =0;

	virtual void setCallbacks() {}

	// Each derived state must have a private static string s_stateID that is unique to that state and is returned by this function
	virtual std::string getStateID() =0;

protected:
	//typedef void *(Callback)();
	BaseState() {}

	std::vector<BaseObject*> m_gameObjects;
	std::vector<std::string> m_textureIDs;
	//std::vector<Callback> m_callbacks;
};

#endif