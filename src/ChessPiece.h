#ifndef _CHESSPIECE_H
#define _CHESSPIECE_H

#include "Constants.h"
#include "BaseObject.h"
#include "GameObjectFactory.h"

#include <SDL.h>

enum piece_type {
	QUEEN = 0,
	KING = 1,
	ROOK = 2,
	PAWN = 3,
	BISHOP = 4,
	KNIGHT = 5
};

enum color {
	WHITE = 0,
	BLACK = 1
};

class ChessPiece : public BaseObject {

public:
	ChessPiece() {
		m_type = PAWN;
		m_color = WHITE;
		m_sourceRect.x = m_type * SPRITE_PIECE_SIZE;
		m_sourceRect.y = 0;
		m_sourceRect.w = m_sourceRect.h = SPRITE_PIECE_SIZE;
		m_inOriginalPosition = true;
	}

	virtual void load(LoaderParams* pParams, piece_type t, color c) {

		BaseObject::load(pParams);

		m_type = t;
		m_color = c;
		m_tileX = m_x % TILE_SIZE;
		m_tileY = m_y % TILE_SIZE;

		m_sourceRect.x = m_type * SPRITE_PIECE_SIZE;
		m_sourceRect.y = m_color * SPRITE_PIECE_SIZE;
	}

	virtual void render();
	virtual void update();

	void setTilePos(int tileX, int tileY) {
		m_tileX = tileX;
		m_tileY = tileY;
		m_x = m_tileX * TILE_SIZE;
		m_y = m_tileY * TILE_SIZE;
	}

	void setPos(int x, int y) {
		m_x = x;
		m_y = y;
	}

	void setType(piece_type t) {
		m_type = t;
		m_sourceRect.x = m_type * SPRITE_PIECE_SIZE;
	}

	int getX() const { return m_x; }
	int getY() const { return m_y; }
	int getTileX() const { return m_tileX; }
	int getTileY() const { return m_tileY; }
	int getType() const { return (int) m_type; }
	int getColor() const { return (int) m_color; }
	bool inOriginalPosition() const { return m_inOriginalPosition; }
	void moved() { m_inOriginalPosition = false; }

private:
	SDL_Rect m_sourceRect;
	piece_type m_type;
	color m_color;

	int m_tileX;
	int m_tileY;
	bool m_inOriginalPosition;
};

class ChessPieceCreator : public BaseCreator {

public:
	virtual BaseObject* createGameObject() const { return new ChessPiece(); }
};

#endif