#include "PlayState.h"

#include "Constants.h"
#include "GameObjectFactory.h"
#include "InputHandler.h"
#include "TextureManager.h"
#include "Vector2D.h"

#include <SDL.h>
#include <iostream>

std::string PlayState::s_stateID = "VERSUSPLAY";

void PlayState::setPiece(piece_type t, color c, int tileX, int tileY, int index) {
	ChessPiece* pPiece = static_cast<ChessPiece*>(m_gameObjects[index]);
	pPiece->load(new LoaderParams(tileX*TILE_SIZE, tileY*TILE_SIZE, TILE_SIZE, TILE_SIZE, "pieces"), t, c);
	m_board[tileX][tileY] = pPiece;
}

void PlayState::onEnter() {

	StateParser::parseState("res/StateData.xml", s_stateID, &m_gameObjects, &m_textureIDs);

	GameObjectFactory::registerType("ChessPiece", new ChessPieceCreator());

	// Initialize the center of the board
	for (int i = 2; i < 6; i++) {
		for (int j = 0; j < 8; j++) {
			m_board[j][i] = 0;
		}
	}

	m_heldPiece = 0;
	m_currentTurn = WHITE;

	// Create chess pieces
	for (int i = 0; i < 32; i++) {
		BaseObject* pObject = GameObjectFactory::create("ChessPiece");
		m_gameObjects.push_back(pObject);
	}
	// Set up white pawn row
	for (int i = 0; i < 8; i++) {
		setPiece(PAWN, WHITE, i, 6, i);
	}
	// Set up black pawn row
	for (int i = 8; i < 16; i++) {
		setPiece(PAWN, BLACK, i-8, 1, i);
	}
	// Set up rooks
	setPiece(ROOK, WHITE, 0, 7, 16);
	setPiece(ROOK, WHITE, 7, 7, 17);
	setPiece(ROOK, BLACK, 0, 0, 18);
	setPiece(ROOK, BLACK, 7, 0, 19);
	// Set up knights
	setPiece(KNIGHT, WHITE, 1, 7, 20);
	setPiece(KNIGHT, WHITE, 6, 7, 21);
	setPiece(KNIGHT, BLACK, 1, 0, 22);
	setPiece(KNIGHT, BLACK, 6, 0, 23);
	// Set up bishops
	setPiece(BISHOP, WHITE, 2, 7, 24);
	setPiece(BISHOP, WHITE, 5, 7, 25);
	setPiece(BISHOP, BLACK, 2, 0, 26);
	setPiece(BISHOP, BLACK, 5, 0, 27);
	// Set up queens
	setPiece(QUEEN, WHITE, 3, 7, 28);
	setPiece(QUEEN, BLACK, 3, 0, 29);
	// Set up kings
	setPiece(KING, WHITE, 4, 7, 30);
	setPiece(KING, BLACK, 4, 0, 31);
}

void PlayState::onExit() {

	GameObjectFactory::destroyType("ChessPiece");
}

void PlayState::render() {
	
	TextureManager::draw("board", 0, 0, 8*TILE_SIZE, 8*TILE_SIZE);
	for (size_t i = 0; i < m_gameObjects.size(); i++) {
		m_gameObjects[i]->render();
	}
}

void PlayState::update() {
	Vector2D mousePos = InputHandler::getMousePosition();
	int tileX = (int)mousePos.getX() / TILE_SIZE;
	int tileY = (int)mousePos.getY() / TILE_SIZE;

	if (InputHandler::mouseButtonDown(MOUSE_BUTTON_LEFT)) {
		if (m_heldPiece == 0) {
			// mouse cursor is over the board
			if (mousePos.getX() <= (float)(8*TILE_SIZE)) {
				// not carrying a piece currently
				if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() == m_currentTurn) {
					// pick the piece up
					m_heldPiece = m_board[tileX][tileY];
					m_board[tileX][tileY] = 0;
					m_origTileX = tileX;
					m_origTileY = tileY;
					// change the rendering order so the held piece renders last (on top of everything else)
					for (std::vector<BaseObject*>::iterator it = m_gameObjects.begin(); it != m_gameObjects.end(); it++) {
						if (m_heldPiece == *it) {
							m_gameObjects.erase(it);
							m_gameObjects.push_back(m_heldPiece);
							break;
						}
					}
				}
			} 
		} else {
			// player is carrying a piece, so piece follows mouse cursor
			m_heldPiece->setPos((int)mousePos.getX() - m_heldPiece->getWidth()/2, (int)mousePos.getY() - m_heldPiece->getHeight()/2);
		}
	}

	if (m_heldPiece != 0 && !InputHandler::mouseButtonDown(MOUSE_BUTTON_LEFT)) {
		checkMove(tileX, tileY);
	}
}

void PlayState::killPieceAt(int tileX, int tileY) {
	ChessPiece* pPiece = m_board[tileX][tileY];
	if (pPiece == 0 || pPiece->getColor() == m_heldPiece->getColor())
		return;
	std::cout << pPiece->getColor() << " loses a " << pPiece->getType() << " at (" << pPiece->getTileX() << ", " << pPiece->getTileY() << ")\n";
	for (std::vector<BaseObject*>::iterator it = m_gameObjects.begin(); it != m_gameObjects.end(); it++) {
		if (pPiece == *it) {
			m_gameObjects.erase(it);
			break;
		}
	}
	m_board[tileX][tileY] = 0;
	delete pPiece;
}

void PlayState::checkMove(int tileX, int tileY) {

	bool castleLeft = false, castleRight = false;

	bool validMove = true;
	if (tileX == m_origTileX && tileY == m_origTileY)
		validMove = false;
	if (tileX > 7)
		validMove = false;
	if (validMove) {
		// Check move based on piece type
		switch(m_heldPiece->getType()) {
		case PAWN:
			validMove = checkPawnMove(tileX, tileY);
			break;
		case ROOK:
			validMove = checkRookMove(tileX, tileY, &castleLeft, &castleRight);
			if (validMove) {
				if (castleLeft) {
					ChessPiece* pKing = m_board[tileX][tileY];
					pKing->setTilePos(2, tileY);
					m_board[tileX][tileY] = 0;
					m_board[2][tileY] = pKing;
					tileX--;
				} else if (castleRight) {
					ChessPiece* pKing = m_board[tileX][tileY];
					pKing->setTilePos(6, tileY);
					m_board[tileX][tileY] = 0;
					m_board[6][tileY] = pKing;
					tileX++;
				}
			}
			break;
		case KNIGHT:
			validMove = checkKnightMove(tileX, tileY);
			break;
		case BISHOP:
			validMove = checkBishopMove(tileX, tileY);
			break;
		case QUEEN:
			validMove = checkQueenMove(tileX, tileY);
			break;
		case KING:
			validMove = checkKingMove(tileX, tileY, &castleLeft, &castleRight);
			if (validMove) {
				if (castleLeft) {
					ChessPiece* pRook = m_board[tileX][tileY];
					pRook->setTilePos(3, tileY);
					m_board[tileX][tileY] = 0;
					m_board[3][tileY] = pRook;
					tileX = 2;
				} else if (castleRight) {
					ChessPiece* pRook = m_board[tileX][tileY];
					pRook->setTilePos(5, tileY);
					m_board[tileX][tileY] = 0;
					m_board[5][tileY] = pRook;
					tileX = 6;
				}
			}
			break;
		}
	}

	if (validMove) {
		m_heldPiece->moved();
		killPieceAt(tileX, tileY);
	} else {
		tileX = m_origTileX;
		tileY = m_origTileY;
	}
	m_heldPiece->setTilePos(tileX, tileY);
	m_board[tileX][tileY] = m_heldPiece;
	m_heldPiece = 0;
	if (validMove) {
		if (check(m_currentTurn) == NOCHECK) // if true, then current color wins
			flipTurn();
	}
}

bool PlayState::checkPawnMove(int tileX, int tileY, bool* killMove) {
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	int direction;
	if (m_currentTurn == WHITE) {
		// pawn has to move up, so make direction negative
		direction = -1;
	} else {
		// pawn has to move down, so make direction positive
		direction = 1;
	}

	// attempt to move a pawn two spaces from its original position
	if (m_heldPiece->inOriginalPosition() && tileX == m_origTileX && tileY == m_origTileY+2*direction && m_board[m_origTileX][m_origTileY+direction] == 0 && m_board[tileX][tileY] == 0) {
		return true;
	}
	// attempt to move a pawn one space forward
	if (tileX == m_origTileX && tileY == m_origTileY+direction && m_board[tileX][tileY] == 0) {
		if ((m_heldPiece->getColor() == WHITE && tileY == 0) || (m_heldPiece->getColor() == BLACK && tileY == 7)) {
			// Turn the pawn into a queen for now
			m_heldPiece->setType(QUEEN);
		}
		return true;
	}
	// attempt to kill an enemy piece diagonally
	if ((tileX == m_origTileX+1 || tileX == m_origTileX-1) && tileY == m_origTileY+direction && m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor()) {
		if (killMove != 0)	
			*killMove = true;
		return true;
	}
	return false;
}

bool PlayState::checkRookMove(int tileX, int tileY, bool* castleLeft, bool* castleRight, bool* killMove) {
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	// attempt to castle
	if (m_heldPiece->inOriginalPosition() && m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getType() == KING
		&& m_board[tileX][tileY]->getColor() == m_heldPiece->getColor() && m_board[tileX][tileY]->inOriginalPosition() && tileY == m_origTileY) {
		if (tileX < m_origTileX) {
			// Right castle
			for (int i = m_origTileX; i > tileX; i--) {
				if (m_board[i][tileY] != 0)
					return false;
			}
			*castleRight = true;
		} else {
			// Left castle
			for (int i = m_origTileX; i < tileX; i++) {
				if (m_board[i][tileY] != 0)
					return false;
			}
			*castleLeft = true;
		}
		return true;
	}
	
	if (tileX != m_origTileX && tileY != m_origTileY)
		return false;
	if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() == m_heldPiece->getColor())
		return false;

	// attempt to move
	int east = 0, south = 0, distance = 0;
	if (tileX > m_origTileX) {
		east = 1;
	} else if (tileX < m_origTileX) {
		east = -1;
	}
	if (tileY > m_origTileY) {
		south = 1;
	} else if (tileY < m_origTileY) {
		south = -1;
	}

	distance = abs(tileX-m_origTileX + tileY-m_origTileY); // one of the differences will always be zero

	for (int i = 0; i < distance; i++) {
		if (m_board[m_origTileX+east*i][m_origTileY+south*i] != 0)
			return false;
	}
	
	if (killMove != 0)
		if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor())
			*killMove = true;

	return true;
}

bool PlayState::checkKnightMove(int tileX, int tileY, bool* killMove) {
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() == m_heldPiece->getColor()) {
		// can't move onto friendly piece
		return false;
	}

	if ((tileX == m_origTileX+1 && tileY == m_origTileY+2) ||
		(tileX == m_origTileX-1 && tileY == m_origTileY+2) ||
		(tileX == m_origTileX+1 && tileY == m_origTileY-2) ||
		(tileX == m_origTileX-1 && tileY == m_origTileY-2) ||
		(tileX == m_origTileX+2 && tileY == m_origTileY+1) ||
		(tileX == m_origTileX-2 && tileY == m_origTileY+1) ||
		(tileX == m_origTileX+2 && tileY == m_origTileY-1) ||
		(tileX == m_origTileX-2 && tileY == m_origTileY-1)) {
			if (killMove != 0)
				if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor())
					*killMove = true;
			return true;
	} else {
		return false;
	}
}

bool PlayState::checkBishopMove(int tileX, int tileY, bool* killMove) {
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	if (abs(tileX-m_origTileX) != abs(tileY-m_origTileY))
		return false;
	if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() == m_heldPiece->getColor())
		return false;

	int distance = abs(tileX - m_origTileX);
	int east, south;
	if (tileX > m_origTileX) {
		east = 1;
	} else {
		east = -1;
	}
	if (tileY > m_origTileY) {
		south = 1;
	} else {
		south = -1;
	}
	for (int i = 0; i < distance; i++) {
		if (m_board[m_origTileX+i*east][m_origTileY+i*south] != 0)
			return false;
	}

	if (killMove != 0)
		if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor())
			*killMove = true;

	return true;
}

bool PlayState::checkQueenMove(int tileX, int tileY, bool* killMove) {\
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	bool validMove, castleLeft, castleRight;
	castleLeft = castleRight = false;
	validMove = checkBishopMove(tileX, tileY) || checkRookMove(tileX, tileY, &castleLeft, &castleRight);
	if (castleLeft || castleRight)
		return false;
	if (killMove != 0)
		if (validMove)
			if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor())
				*killMove = true;
	return validMove;
}

bool PlayState::checkKingMove(int tileX, int tileY, bool* castleLeft, bool* castleRight, bool* killMove) {
	if (tileX < 0 || tileX > 7 || tileY < 0 || tileY > 7)
		return false;

	// attempt to castle
	if (m_heldPiece->inOriginalPosition() && m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getType() == ROOK
		&& m_board[tileX][tileY]->getColor() == m_heldPiece->getColor() && m_board[tileX][tileY]->inOriginalPosition() && tileY == m_origTileY) {
		if (tileX < m_origTileX) {
			// Left castle
			for (int i = m_origTileX; i > tileX; i--) {
				if (m_board[i][tileY] != 0)
					return false;
			}
			*castleLeft = true;
		} else {
			// Right castle
			for (int i = m_origTileX; i < tileX; i++) {
				if (m_board[i][tileY] != 0)
					return false;
			}
			*castleRight = true;
		}
		return true;
	}

	// attempt to move in any of the 8 directions
	if (abs(tileX - m_origTileX) > 1 || abs(tileY-m_origTileY) > 1 || (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() == m_heldPiece->getColor()))
		return false;

	if (killMove != 0)
		if (m_board[tileX][tileY] != 0 && m_board[tileX][tileY]->getColor() != m_heldPiece->getColor())
			*killMove = true;
	return true;
}

check_condition PlayState::check(color currentTurnColor) {

	check_condition cond = NOCHECK;
	std::vector<ChessPiece*> pieces; // store all of our pieces in a temporary vector

	// find opponent king's location
	// if the piece is ours, store it in pieces
	int x, y;
	ChessPiece* pKing = 0;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			if (m_board[i][j] != 0) {
				if (m_board[i][j]->getColor() != currentTurnColor) {
					if (m_board[i][j]->getType() == KING) {
						x = i;
						y = j;
						pKing = m_board[i][j];
					}
				} else {
					pieces.push_back(m_board[i][j]);
				}
			}
		}
	}

	bool killMove = false, checkMate = true; // we have to prove checkMate is false

	// check all directions
	int x = pKing->getTileX();
	int y = pKing->getTileY();
	for (int i = -1; i <= 1; i++) {
		for (int j = -1; j <= 1; j++) {
			for (int k = 0; k < pieces.size(); k++) {
				// we need to temporarily set m_heldPiece in order to use each pieces move check function
				m_heldPiece = pieces[k];
				switch (m_heldPiece->getType()) {
				case PAWN:
					if (checkPawnMove(x+i, y+j, &killMove) && killMove == true)
						cond = CHECK;
				}
			}
		}
	}
	return cond;
}