#ifndef _GAMEOBJECTFACTORY_H
#define _GAMEOBJECTFACTORY_H

#include <string>
#include <unordered_map>

class BaseObject;

class BaseCreator {

public:
	virtual BaseObject* createGameObject() const =0;

};

class GameObjectFactory {

public:
	static void registerType(std::string typeID, BaseCreator* pCreator);
	static void destroyType(std::string typeID);
	static BaseObject* create(std::string typeID);

private:
	GameObjectFactory() {}
	~GameObjectFactory();
	static std::unordered_map<std::string, BaseCreator*> s_creators;

};

#endif