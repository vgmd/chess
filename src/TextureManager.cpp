#include "TextureManager.h"

#include "SDLErrorLogger.h"

#include <SDL_image.h>

#include <iostream>

SDL_Renderer* TextureManager::s_pRenderer = 0;
std::unordered_map<std::string, SDL_Texture*> TextureManager::s_textureMap;

int TextureManager::load(const char* filePath, const std::string& textureID) {
	if (s_pRenderer == 0) {
		std::cerr << "TextureManager::load failed: No renderer set\n";
		return -1;
	}

	std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.find(textureID);
	if (it != s_textureMap.end()) {
		// Texture already exists!
		std::cout << "TextureManager::load found an entry for " << textureID << ". Aborting load.\n";
		return 0;
	}

	SDL_Texture* pTexture = IMG_LoadTexture(s_pRenderer, filePath);
	if (pTexture == 0) {
		logSDLError(std::cerr, "IMG_LoadTexture");
		return -2;
	}

	s_textureMap[textureID] = pTexture;

	return 0;
}

void TextureManager::clearFromTextureMap(const std::string& textureID) {
	std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.find(textureID);
	if (it == s_textureMap.end()) {
		std::cout << "TextureManager::clearFromTextureMap : Texture not found: " << textureID << "\n";
		return;
	}

	SDL_DestroyTexture(s_textureMap[textureID]);

	s_textureMap.erase(it);
}

void TextureManager::clean() {
	for (std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.begin(); it != s_textureMap.end(); it++) {
		SDL_DestroyTexture(it->second);
	}
	s_textureMap.clear();
	IMG_Quit();
}

void TextureManager::draw(const std::string& textureID, const SDL_Rect& src, const SDL_Rect& dst, const SDL_RendererFlip flip, const double angle, const SDL_Point* center) {
	std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.find(textureID);
	if (it == s_textureMap.end()) {
		return;
	}

	SDL_RenderCopyEx(s_pRenderer, s_textureMap[textureID], &src, &dst, angle, center, flip);
}

void TextureManager::draw(const std::string& textureID, const int x, const int y, const int w, const int h, const SDL_RendererFlip flip, const double angle, const SDL_Point* center) {
	std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.find(textureID);
	if (it == s_textureMap.end()) {
		return;
	}

	SDL_Rect src;
	SDL_Rect dst;

	src.x = 0;
	src.y = 0;
	src.w = dst.w = w;
	src.h = dst.h = h;
	dst.x = x;
	dst.y = y;

	SDL_RenderCopyEx(s_pRenderer, s_textureMap[textureID], &src, &dst, angle, center, flip);
}

void TextureManager::drawFrame(const std::string& textureID, const int x, const int y, const int w, const int h, const int currentRow, const int currentFrame, const SDL_RendererFlip flip, const double angle, const SDL_Point* center) {
	std::unordered_map<std::string, SDL_Texture*>::iterator it = s_textureMap.find(textureID);
	if (it == s_textureMap.end()) {
		return;
	}

	SDL_Rect src;
	SDL_Rect dst;

	src.x = w * currentFrame;
	src.y = h * currentRow;
	src.w = dst.w = w;
	src.h = dst.h = h;
	dst.x = x;
	dst.y = y;

	SDL_RenderCopyEx(s_pRenderer, s_textureMap[textureID], &src, &dst, angle, center, flip);
}