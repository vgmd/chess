#include "GameStateMachine.h"

#include "BaseState.h"

#include <iostream>

std::vector<BaseState*> GameStateMachine::s_gameStates;
bool GameStateMachine::s_changingState = false;

void GameStateMachine::pushState(BaseState* pState) {
	if (pState != 0) {
		s_gameStates.push_back(pState);
		s_gameStates.back()->onEnter();
	} else {
		std::cerr << "GameStateMachine::pushState failed: pState is NULL\n";
	}
}

void GameStateMachine::popState() {
	if (!s_gameStates.empty()) {
		s_gameStates.back()->onExit();
		delete s_gameStates.back();
		s_gameStates.pop_back();
	}
}

void GameStateMachine::changeState(BaseState* pState) {
	s_changingState = true;
	if (pState != 0) {
		if (s_gameStates.back()->getStateID() != pState->getStateID())
			return;
		s_gameStates.back()->onExit();
		delete s_gameStates.back();
		s_gameStates.pop_back();
		s_gameStates.push_back(pState);
		s_gameStates.back()->onEnter();
	} else {
		std::cerr << "GameStateMachine::changeState failed: pState is NULL\n";
	}
}

void GameStateMachine::update() {
	if (s_changingState)
		s_changingState = false;
	if (!s_gameStates.empty())
		s_gameStates.back()->update();
}

void GameStateMachine::render() {
	if (!s_gameStates.empty())
		s_gameStates.back()->render();
}