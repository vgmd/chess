#ifndef _GAME_H
#define _GAME_H

#include "Constants.h"

#include <iostream>

struct SDL_Window;
struct SDL_Renderer;

class Game {

public:
	Game() {}
	~Game() {}

	int init();
	void render();
	void handleEvents();
	void update();
	void clean();

	/// GETTERS
	static SDL_Renderer* getRenderer() { return s_pRenderer; }
	bool running() const { return m_running; }

private:
	bool m_running;

	SDL_Window* m_pWindow;
	static SDL_Renderer* s_pRenderer;
};

#endif