#ifndef _STATEPARSER_H
#define _STATEPARSER_H

#include "tinyxml.h"
#include <string>
#include <vector>

class BaseObject;

class StateParser {

public:
	static bool parseState(const char* fileName, std::string stateID, std::vector<BaseObject*>* pObjects, std::vector<std::string>* pTextureIDs);

private:
	StateParser() {}
	~StateParser() {}
	static void parseObjects(TiXmlElement* pObjectRoot, std::vector<BaseObject*>* pObjects);
	static void parseTextures(TiXmlElement* pTextureRoot, std::vector<std::string>* pTextureIDs);

};

#endif