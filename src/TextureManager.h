#ifndef _TEXTUREMANAGER_H
#define _TEXTUREMANAGER_H

#include <SDL.h>

#include <string>
#include <unordered_map>

class TextureManager {

public:
	static int load(const char* filePath, const std::string& textureID);
	static void clearFromTextureMap(const std::string& textureID);
	static void clean();

	/// Rendering functions
	static void draw(const std::string& textureID, const SDL_Rect& src, const SDL_Rect& dst, const SDL_RendererFlip flip = SDL_FLIP_NONE, const double angle = 0, const SDL_Point* center = 0);
	static void draw(const std::string& textureID, const int x, const int y, const int w, const int h, const SDL_RendererFlip flip = SDL_FLIP_NONE, const double angle = 0, const SDL_Point* center = 0);
	static void drawFrame(const std::string& textureID, const int x, const int y, const int w, const int h, const int currentRow, const int currentFrame, const SDL_RendererFlip flip = SDL_FLIP_NONE, const double angle = 0, const SDL_Point* center = 0);

	/// Setters
	static void setRenderer(SDL_Renderer* pRenderer) { s_pRenderer = pRenderer; }
	
	/// Getters
	static SDL_Renderer* getRenderer() { return s_pRenderer; }

private:
	TextureManager() {}
	~TextureManager() {}

	static SDL_Renderer* s_pRenderer;
	static std::unordered_map<std::string, SDL_Texture*> s_textureMap;
};

#endif