#include "Game.h"

int main(int argc, char* argv[]) {
	int returnCode;
	Game game;

	returnCode = game.init();
	if (returnCode != 0)
		return returnCode;

	while (game.running()) {
		game.render();
		game.handleEvents();
		game.update();
	}

	game.clean();
	return 0;
}