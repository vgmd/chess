#ifndef _LOADERPARAMS_H
#define _LOADERPARAMS_H

#include <string>

class LoaderParams {

public:
	LoaderParams(int X, int Y, int W, int H, std::string TextureID, int NumFrames = 1, int AnimSpeed = 0, int CallbackID = 0) {
		x = X;
		y = Y;
		width = W;
		height = H;
		textureID = TextureID;
		numFrames = NumFrames;
		animSpeed = AnimSpeed;
		callbackID = CallbackID;
	}
	~LoaderParams() {}
	
	int x;
	int y;
	int width;
	int height;
	int numFrames;
	int callbackID;
	int animSpeed;
	std::string textureID;
};

#endif