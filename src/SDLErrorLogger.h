#ifndef _SDLERRORLOGGER_H
#define _SDLERRORLOGGER_H

#include <SDL_error.h>

#include <iostream>

inline void logSDLError(std::ostream& out, const char* functionName) {
	out << functionName << " failed: " << SDL_GetError() << "\n";
}

#endif