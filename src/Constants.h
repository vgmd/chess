#ifndef _CONSTANTS_H
#define _CONSTANTS_H

static const char* WINDOW_TITLE = "Chess";
static const int WINDOW_WIDTH = 1280;
static const int WINDOW_HEIGHT = 800;
static const int TILE_SIZE = 100;
static const int SPRITE_PIECE_SIZE = 21;

#endif