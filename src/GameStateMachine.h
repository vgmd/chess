#ifndef _GAMESTATEMACHINE_H
#define _GAMESTATEMACHINE_H

#include <vector>

class BaseState;

class GameStateMachine {

public:
	static void update();
	static void render();

	static void pushState(BaseState* pState);
	static void changeState(BaseState* pState);
	static void popState();

	static bool changingState() { return s_changingState; }

private:
	GameStateMachine() {}
	~GameStateMachine() {}

	static std::vector<BaseState*> s_gameStates;
	static bool s_changingState;
};

#endif