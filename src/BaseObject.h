#ifndef _BASEOBJECT_H
#define _BASEOBJECT_H

#include "LoaderParams.h"
#include "GameObjectFactory.h"

#include <string>

class BaseObject {

public:

	virtual ~BaseObject() {}

	virtual void render() =0;
	virtual void update() =0;

	int getWidth() const { return m_width; }
	int getHeight() const { return m_height; }

	virtual void load(LoaderParams* pParams) {
		m_x = pParams->x;
		m_y = pParams->y;
		m_width = pParams->width;
		m_height = pParams->height;
		m_textureID = pParams->textureID;
		m_numFrames = pParams->numFrames;
		m_callbackID = pParams->callbackID;
		m_animSpeed = pParams->animSpeed;
		delete pParams;
	}

protected:
	BaseObject() {}

	int m_x;
	int m_y;
	int m_width;
	int m_height;
	int m_numFrames;
	int m_callbackID;
	int m_animSpeed;
	std::string m_textureID;
};

#endif