#ifndef _INPUTHANDLER_H
#define _INPUTHANDLER_H

#include <SDL.h>

class Vector2D;

enum {
	MOUSE_BUTTON_LEFT = 0,
	MOUSE_BUTTON_RIGHT = 1
};

class InputHandler {

public:
	static void handleEvents();

	/// GETTERS
	static bool isKeyDown(SDL_Keycode);
	static bool mouseButtonDown(int which) { return s_mouseButtonDown[which]; }
	static const Vector2D& getMousePosition() { return m_mousePos; }
	static bool getQuit() { return s_quit; }

private:
	InputHandler() {}
	~InputHandler() {}

	static void onMouseButtonDown(SDL_Event&);
	static void onMouseButtonUp(SDL_Event&);
	static void onMouseMove(SDL_Event&);

	static bool s_mouseButtonDown[];
	static bool s_quit;
	static const Uint8* m_keys;
	static Vector2D m_mousePos;
};

#endif