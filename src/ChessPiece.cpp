#include "ChessPiece.h"

#include "TextureManager.h"

void ChessPiece::render() {
	SDL_Rect dst;
	dst.x = m_x;
	dst.y = m_y;
	dst.w = dst.h = TILE_SIZE;
	TextureManager::draw(m_textureID, m_sourceRect, dst);
}

void ChessPiece::update() {}