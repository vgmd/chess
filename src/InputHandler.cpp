#include "InputHandler.h"

#include "Vector2D.h"

#include <iostream>

bool InputHandler::s_mouseButtonDown[] = {false, false};
bool InputHandler::s_quit = false;
const Uint8* InputHandler::m_keys = SDL_GetKeyboardState(0);
Vector2D InputHandler::m_mousePos(0,0);

void InputHandler::handleEvents() {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch(event.type) {
		case SDL_QUIT:
			s_quit = true;
			return;
		case SDL_MOUSEBUTTONDOWN:
			onMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseButtonUp(event);
			break;
		case SDL_MOUSEMOTION:
			onMouseMove(event);
			break;
		}
	}
}

bool InputHandler::isKeyDown(SDL_Keycode key) {
	if (m_keys != 0) {
		SDL_Scancode scan = SDL_GetScancodeFromKey(key);
		if (m_keys[scan] == 1)
			return true;
		else
			return false;
	} else {
		std::cerr << "InputHandler::isKeyDown error: m_keys is NULL\n";
		return false;
	}
}

void InputHandler::onMouseButtonDown(SDL_Event& event) {
	if (event.button.button == SDL_BUTTON_LEFT)
		s_mouseButtonDown[MOUSE_BUTTON_LEFT] = true;
	if (event.button.button == SDL_BUTTON_RIGHT)
		s_mouseButtonDown[MOUSE_BUTTON_RIGHT] = true;
}

void InputHandler::onMouseButtonUp(SDL_Event& event) {
	if (event.button.button == SDL_BUTTON_LEFT)
		s_mouseButtonDown[MOUSE_BUTTON_LEFT] = false;
	if (event.button.button == SDL_BUTTON_RIGHT)
		s_mouseButtonDown[MOUSE_BUTTON_RIGHT] = false;
}

void InputHandler::onMouseMove(SDL_Event& event) {
	m_mousePos.setX(event.motion.x);
	m_mousePos.setY(event.motion.y);
}